FROM openjdk:8-jdk-alpine as builder
RUN apk add --no-cache maven
WORKDIR /app
COPY . .
RUN mvn package

FROM openjdk:8-jre-alpine as runtime
WORKDIR /app
COPY --from=builder /app/target/spring-docker-1.0.0.jar .
ENTRYPOINT [ "java", "-jar", "spring-docker-1.0.0.jar"]
